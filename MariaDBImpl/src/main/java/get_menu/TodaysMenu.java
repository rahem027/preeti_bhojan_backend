package get_menu;

import entities.MenuItem;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TodaysMenu implements use_cases.get_menu.TodaysMenu {

    private final String DATABASE, USERNAME, PASSWORD, TABLE;

    public TodaysMenu(String database, String username, String password, String table) {
        this.DATABASE = database;
        this.USERNAME = username;
        this.PASSWORD = password;
        this.TABLE = table;
    }

    @Override
    public List<MenuItem> get(String mealType) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement statement = connection.prepareStatement("select * from " + TABLE + " where Type = ? and Date = CURDATE()")) {

                statement.setString(1, mealType);


                List<MenuItem> result = new ArrayList<>();

                ResultSet resultSet = statement.executeQuery();

                while(resultSet.next()) {
                    result.add(new MenuItem(resultSet.getString("ID"), resultSet.getString("Name"), resultSet.getInt("Price"), resultSet.getInt("Quantity")));
                }

                return result;
            }
        }

    }
}

package add_customer;

import entities.Customer;

import java.sql.*;
import java.util.UUID;

public class Customers implements use_cases.add_customer.Customers {

    private final String DATABASE, USERNAME, PASSWORD, TABLE;

    public Customers(String database, String username, String password, String table) {
        DATABASE = database;
        USERNAME = username;
        PASSWORD = password;
        TABLE = table;
    }

    @Override
    public boolean exists(String phoneNumber) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement statement = connection.prepareStatement("select PhoneNumber from " + TABLE + " where PhoneNumber = ?")) {
                statement.setString(1, phoneNumber);

                ResultSet resultSet = statement.executeQuery();

                return resultSet.next();
            }
        }

    }

    @Override
    public void add(Customer customer, char[] hashedPassword) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {

            final String insertQuery = "insert into " + TABLE + "(PhoneNumber, PasswordHash, Name, Company, Address) values (?, ?, ?, ?, ?)";

            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                String id = UUID.randomUUID().toString();

                statement.setString(1, customer.getPhoneNumber());
                statement.setString(2, String.valueOf(hashedPassword));
                statement.setString(3, customer.getName());
                statement.setString(4, customer.getAddress());
                statement.setString(5, customer.getCompany());


                statement.executeUpdate();
            }
        }

    }
}

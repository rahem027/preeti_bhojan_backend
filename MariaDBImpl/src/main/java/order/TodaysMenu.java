package order;

import java.sql.*;
import java.util.Optional;

public class TodaysMenu implements use_cases.order.TodaysMenu {
    private final String DATABASE, USERNAME, PASSWORD, TABLE;

    public TodaysMenu(String DATABASE, String USERNAME, String PASSWORD, String TABLE) {
        this.DATABASE = DATABASE;
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
        this.TABLE = TABLE;
    }

    @Override
    public Optional<Integer> getQuantity(String id) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select Quantity from " + TABLE + " where ID = ? and Date = CURDATE()")) {
                preparedStatement.setString(1, id);

                ResultSet resultSet = preparedStatement.executeQuery();

                if (!resultSet.next() ) {
                    return Optional.empty();
                }

                return Optional.of(resultSet.getInt("Quantity"));
            }
        }
    }

    @Override
    public void updateQuantity(String id, int quantity) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("update " + TABLE + " set Quantity = ? where ID = ?")) {
                preparedStatement.setInt(1, quantity);
                preparedStatement.setString(2, id);

                if (preparedStatement.executeUpdate() != 1) {
                    throw new SQLException();
                }
            }
        }
    }

    @Override
    public String getMealType(String id) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("select Type from " + TABLE + " where ID = ?")) {
                preparedStatement.setString(1, id);

                ResultSet resultSet = preparedStatement.executeQuery();

                if (!resultSet.next() ) {
                    throw new SQLException();
                }

                return resultSet.getString("Type");
            }
        }
    }
}

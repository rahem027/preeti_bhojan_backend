package order;

import java.sql.*;
import java.time.LocalDate;
import java.util.UUID;

public class TodaysOrders implements use_cases.order.TodaysOrders {

    private final String USERNAME, PASSWORD, DATABASE, TABLE;

    public TodaysOrders(String database, String username, String password, String table) {
        USERNAME = username;
        PASSWORD = password;
        DATABASE = database;
        TABLE = table;
    }

    @Override
    public String add(String menuID, int quantity) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement statement = connection.prepareStatement("insert into " + TABLE + " values(?, ?, ?, ?)")) {
                UUID orderID = UUID.randomUUID();

                statement.setString(1, orderID.toString());
                statement.setString(2, menuID);
                statement.setInt(3, quantity);
                statement.setDate(4, Date.valueOf(LocalDate.now()));

                statement.executeUpdate();

                return orderID.toString();
            }
        }
    }
}

package authenticate;

import java.sql.*;

public class Customers implements use_cases.authenticate.Customers {

    private final String USERNAME, PASSWORD, DATABASE, TABLE;

    public Customers(String database, String username, String password, String table) {
        USERNAME = username;
        PASSWORD = password;
        DATABASE = database;
        TABLE = table;
    }

    @Override
    public boolean doesNotExist(String id) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {

            // Select exists () clause was slightly unreadable. Hence selecting ID directly.

            try (PreparedStatement statement = connection.prepareStatement("select ID from " + TABLE +" where ID = ?")) {
                statement.setString(1, id);

                ResultSet resultSet = statement.executeQuery();

                // I assume that the ID is a primary key. Hence if the query returned a result, the said row exists which means the user exists
                return !resultSet.next();
            }
        }

    }


    @Override
    public char[] getPasswordHash(String id) throws SQLException {

        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (PreparedStatement statement = connection.prepareStatement("select PasswordHash from " + TABLE + " where ID = ?")) {
                statement.setString(1, id);

                ResultSet resultSet = statement.executeQuery();

                if (!resultSet.next()) {
                    throw new SQLException("Could not select");
                }

                return resultSet.getString("PasswordHash").toCharArray();
            }
        }
    }
}

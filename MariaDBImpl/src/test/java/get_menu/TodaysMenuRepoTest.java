package get_menu;

import entities.MenuItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TodaysMenuRepoTest {

    private static final String USERNAME = "test", PASSWORD = "", DATABASE = "MenuRepoTest", TABLE = "Customers";
    private TodaysMenu todaysMenu = new TodaysMenu(DATABASE, USERNAME, PASSWORD, TABLE);

    private void insert(PreparedStatement statement, String name, String mealType, LocalDate date) throws SQLException {
        statement.setString(1, UUID.randomUUID().toString());
        statement.setString(2, name);
        statement.setInt(3, 10);
        statement.setInt(4, 10);
        statement.setString(5, mealType);
        statement.setDate(6, Date.valueOf(date));

        statement.executeUpdate();
    }

    @BeforeEach
    void initDB() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/", USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("drop database if exists " + DATABASE );

                statement.execute("create database " + DATABASE);

                String query = "create table " + DATABASE + "." + TABLE + " (" +
                        "ID varchar(40) PRIMARY KEY," +
                        "Name text not null," +
                        "Price int not null," +
                        "Quantity int not null," +
                        "Type text not null," +
                        "Date date not null )";

                statement.execute(query);

                try (PreparedStatement statement1 = connection.prepareStatement("insert into " + DATABASE + "." + TABLE + " values (?, ?, ?, ?, ?, ?)")) {

                    insert(statement1, "test1", "a", LocalDate.now());
                    insert(statement1, "test2", "a", LocalDate.now().minusDays(1));

                    insert(statement1, "test3", "b", LocalDate.now());
                    insert(statement1, "test4", "b", LocalDate.now().minusDays(1));
                }
            }
        }
    }

    private static Stream<Arguments> getNameAndMealType() {
        return Stream.of(
                Arguments.of("test1", "a"),
                Arguments.of("test3", "b")
        );
    }
    @ParameterizedTest
    @MethodSource("getNameAndMealType")
    void should_only_return_correct_item_when_asked_for_meal_type(String name, String mealType) throws SQLException {
        List<MenuItem> result = todaysMenu.get(mealType);

        assertEquals(1, result.size());
        assertEquals(name, result.get(0).getName());
    }
}

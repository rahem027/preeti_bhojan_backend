package add_customer;

import entities.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerRepoTest {
    private static final String USERNAME = "test", PASSWORD = "", DATABASE = "AddCustomerTest", TABLE = "Customers";
    private static final String INSERTED_PHONE_NUMBER = "1";

    private static final String TEST = "test";

    Customers customers = new Customers(DATABASE, USERNAME, PASSWORD, TABLE);

    @BeforeEach
    void initializeDB() {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306", USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("drop database if exists " + DATABASE);

                statement.execute("create database " + DATABASE);

                statement.execute("create table " + DATABASE + "." + TABLE + " (" +
                        "PhoneNumber varchar(20) PRIMARY KEY," +
                        "PasswordHash text not null, " +
                        "Name text not null, " +
                        "Address text not null," +
                        "Company text not null)");

                try (PreparedStatement statement1 = connection.prepareStatement("insert into " + DATABASE + "." + TABLE + " values(?, ?, ?, ?, ?)") ) {
                    statement1.setString(1, INSERTED_PHONE_NUMBER);
                    statement1.setString(2, TEST);
                    statement1.setString(3, TEST);
                    statement1.setString(4, TEST);
                    statement1.setString(5, TEST);

                    statement1.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void exists_should_return_true_when_phone_number_exists() throws SQLException {
        assertTrue(customers.exists(INSERTED_PHONE_NUMBER));
    }

    @Test
    void exists_should_return_false_when_phone_number_does_not_exist_in_db() throws SQLException {
        assertFalse(customers.exists(""));
    }

    @Test
    void add_should_add_all_the_rows() throws SQLException {
        final String phoneNumber = "12390878";
        customers.add(new Customer("temp", "temp", "temp", phoneNumber), "temp".toCharArray());

        assertTrue(customers.exists(phoneNumber));
    }
}

package authenticate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerRepoTest {

    private static final String USERNAME = "test", PASSWORD = "", DATABASE = "AuthenticateTest", TABLE = "Customers";
    private static final String INSERTED_PASsWORD_HASH = "0e19uh9er jnf38ynf";
    private static final String INSERTED_ID = "2";

    Customers customers;

    @BeforeEach
    void initializeCustomerRepo() {
        customers = new Customers(DATABASE, USERNAME, PASSWORD, TABLE);
    }

    @BeforeAll
    static void beforeAll() {

        final int tempID = 1;
        final String tempPasswordHash = "98t1yuejhb715te";

        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306", USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("drop database if exists " + DATABASE);

                statement.execute("create database " + DATABASE);

                statement.execute("create table " + DATABASE + "." + TABLE +" (" +
                        "ID varchar(100) PRIMARY KEY," +
                        "PasswordHash text not null )");

                try (PreparedStatement statement1 = connection.prepareStatement("insert into " + DATABASE + "." + TABLE + " values(?, ?)")) {
                    statement1.setString(1, INSERTED_ID);
                    statement1.setString(2, INSERTED_PASsWORD_HASH);

                    statement1.execute();

                    statement1.setInt(1, tempID);
                    statement1.setString(2, tempPasswordHash);

                    statement1.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void doesNotExist_should_return_true_when_customer_does_not_exist() throws SQLException {
        // random id that should not exist anyway
        assertTrue(customers.doesNotExist("-1"));
    }

    @Test
    void doesNotExist_should_return_false_when_customer_exists() throws SQLException {
        assertFalse(customers.doesNotExist(INSERTED_ID));
    }

    @Test
    void getPassWordHash_should_return_the_right_password_hash() throws SQLException {
        assertArrayEquals(INSERTED_PASsWORD_HASH.toCharArray(), customers.getPasswordHash(INSERTED_ID));
    }

    @Test
    void should_throw_SQLException_when_id_does_not_exist() {
        assertThrows(SQLException.class, () -> customers.getPasswordHash("-1"));
    }
}

package order;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TodaysMenuTest {

    private static final String DATABASE = "OrderTest", USERNAME = "test", PASSWORD = "", TABLE = "Menu";
    private static final String mealType = "a";
    private static int quantity = 15;

    public static final String insertedID1 = "1",
                               insertedID2 = insertedID1 + "10",
                               idNotInserted = insertedID1 + "1001";

    TodaysMenu todaysMenu = new TodaysMenu(DATABASE, USERNAME, PASSWORD, TABLE);

    @BeforeEach
    void initDB() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/", USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("drop database if exists " + DATABASE);

                statement.execute("create database " + DATABASE);

                statement.execute("create table " + DATABASE + "." + TABLE + " (" +
                        "ID text," +
                        "Date date," +
                        "Quantity int," +
                        "Type text)");

                try (PreparedStatement statement1 = connection.prepareStatement("insert into " + DATABASE + "." + TABLE + " values (?, ?, ?, ?)")) {
                    statement1.setString(1, insertedID1);
                    statement1.setDate(2, Date.valueOf(LocalDate.now()));
                    statement1.setInt(3, quantity);
                    statement1.setString(4, mealType);

                    statement1.execute();

                    statement1.setString(1, insertedID2);
                    statement1.setDate(2, Date.valueOf(LocalDate.now().minusDays(1)));
                    statement1.setInt(3, quantity+1);
                    statement1.setString(4, mealType);

                    statement1.execute();
                }
            }
        }
    }

    @Test
    void getQuantity_should_return_empty_optional_when_menuId_does_not_exist() throws SQLException {
        assertTrue(todaysMenu.getQuantity(idNotInserted).isEmpty());
    }

    @Test
    void getQuantity_should_return_empty_optional_when_menuId_has_different_date() throws SQLException {
        assertTrue(todaysMenu.getQuantity(insertedID2).isEmpty());
    }

    @Test
    void updateQuantity_updates_quantity() throws SQLException {
        todaysMenu.updateQuantity(insertedID1, 10);

        assertTrue(todaysMenu.getQuantity(insertedID1).isPresent());
        assertEquals(10, todaysMenu.getQuantity(insertedID1).get());

        todaysMenu.updateQuantity(insertedID1, quantity);
    }

    @Test
    void updateQuantity_throws_SQLException_when_id_does_not_exists() {
        assertThrows(SQLException.class, () -> todaysMenu.updateQuantity(idNotInserted, 0));
    }

    @Test
    void getMealType_returns_correct_meal_type() throws SQLException {
        assertEquals(mealType, todaysMenu.getMealType(insertedID1));
    }

    @Test
    void getMealType_throws_SQLException_when_id_does_not_exists() {
        assertThrows(SQLException.class, () -> todaysMenu.getMealType(idNotInserted));
    }
}
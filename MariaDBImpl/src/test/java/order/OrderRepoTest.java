package order;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class OrderRepoTest {

    private static final String USERNAME = "test", PASSWORD = "", DATABASE = "OrderRepoTest", TABLE = "OrderLog";

    @BeforeEach
    void initDB() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/", USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("drop database if exists " + DATABASE);

                statement.execute("create database " + DATABASE);

                String query = "create table " + DATABASE + "." + TABLE + " (" +
                        "OrderID varchar(40) PRIMARY KEY," +
                        "MenuID varchar(40) unique," +
                        "Quantity int not null," +
                        "Date date not null)";

                statement.execute(query);
            }
        }
    }

    @Test
    void should_add_row_with_all_the_fields_when_add_is_called() throws SQLException {
        TodaysOrders orderLog = new TodaysOrders(DATABASE, USERNAME, PASSWORD, TABLE);

        final String menuId = UUID.randomUUID().toString();
        final int quantity = 10;

        final String orderID = orderLog.add(menuId, quantity);

        try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + DATABASE, USERNAME, PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("select * from " + TABLE);

                // make sure one row exists
                assertTrue(resultSet.next());

                // make sure all rows are valid
                assertEquals(orderID, resultSet.getString("OrderID"));
                assertEquals(menuId, resultSet.getString("MenuID"));
                assertEquals(quantity, resultSet.getInt("Quantity"));
                assertEquals(new Date(System.currentTimeMillis()).toString(), resultSet.getDate("Date").toString());

                // make sure only one row is returned
                assertFalse(resultSet.next());

            }
        }
    }
}

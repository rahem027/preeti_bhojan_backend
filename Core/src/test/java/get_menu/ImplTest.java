package get_menu;

import entities.MenuItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import use_cases.get_menu.*;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StubMenu implements TodaysMenu {
    @Override
    public List<MenuItem> get(String mealType) {
        return null;
    }
}

// The only valid mealType is "a"
class StubConfig implements Config {
    static final String validMealType = "a";
    @Override
    public boolean isNotValid(String mealType) {
        return !mealType.equals(validMealType);
    }
}


public class ImplTest {
    InputBoundary getMenu;

    @BeforeEach
    void init() {
        getMenu = new Factory().getInstance(new StubMenu(), new StubConfig());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_mealType_is_null_or_empty(String mealType) {
        Response response = getMenu.execute(new Request(mealType));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }
    @Test
    void should_error_out_when_meal_type_is_invalid() {
        Response response = getMenu.execute(new Request("abc"));

        assertEquals(BusinessError.InvalidMealType, response.error(), "The error should be InvalidMealType");
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_menu_when_meal_type_is_valid() {
        Response response = getMenu.execute(new Request(StubConfig.validMealType));

        // stub repository returns null when interactor queries it for menu
        assertNull(response.getMenu(), "response.getMenu() should return null because the stub returns null when asked for menu");
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_error_when_get_throws_SQLException() {
        InputBoundary getMenu = new Factory().getInstance(mealType -> {
            throw new SQLException();
        }, new StubConfig());

        Response response = getMenu.execute(new Request(StubConfig.validMealType));
        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
    }
}

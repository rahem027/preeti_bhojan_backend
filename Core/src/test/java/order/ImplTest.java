package order;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import use_cases.authenticate.Customers;
import use_cases.order.*;

import java.sql.SQLException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class Config implements use_cases.order.Config {
    private boolean cannotServe;

    public Config(boolean cannotServe) {
        this.cannotServe = cannotServe;
    }

    @Override
    public boolean cannotServeMealTypeRightNow(String mealType) {
        return cannotServe;
    }
}

class StubCustomers implements Customers {
    final String password = "test123";
    char[] hashedPassword = BCrypt.with(LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2A)).hashToChar(12, password.toCharArray());

    @Override
    public boolean doesNotExist(String phoneNumber) {
        return false;
    }

    @Override
    public char[] getPasswordHash(String phoneNumber) {
        return hashedPassword;
    }
}

class StubTodaysMenu implements TodaysMenu {

    boolean hasQuantity;
    int quantity;
    String mealType;

    public StubTodaysMenu(boolean hasQuantity, int quantity, String mealType) {
        this.hasQuantity = hasQuantity;
        this.quantity = quantity;
        this.mealType = mealType;
    }


    @Override
    public Optional<Integer> getQuantity(String menuID) {
        return hasQuantity? Optional.of(quantity) : Optional.empty();
    }

    @Override
    public void updateQuantity(String menuID, int updatedQuantity) {
        quantity = updatedQuantity;
    }

    @Override
    public String getMealType(String menuID) {
        return mealType;
    }

}

class PhoneNumberValidator implements use_cases.util.PhoneNumberValidator {

    @Override
    public boolean isInvalid(String phoneNumber) {
        return false;
    }
}

public class ImplTest {

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_menu_id_is_null_or_empty(String menuID) {
        InputBoundary order = new Factory().getInstance(null, null, null,null, null);

        Response response = order.execute(new Request("a", "jsah", menuID, 0));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_return_error_when_quantity_is_less_than_zero() {
        InputBoundary order = new Factory().getInstance(null, null, null,null, null);

        Response response = order.execute(new Request("a", "jsah", "a", -1));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_phoneNumber_is_null_or_empty(String phoneNumber) {
        InputBoundary order = new Factory().getInstance(null, null, null,null, null);

        Response response = order.execute(new Request(phoneNumber, "jsah", "a", 0));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_password_is_null_or_empty(String password) {
        InputBoundary order = new Factory().getInstance(null, null, null, null, null);

        Response response = order.execute(new Request("a", password, "a", 0));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_return_error_when_phoneNumber_is_invalid() {
        InputBoundary order = new Factory().getInstance(null, null, phoneNumber -> true, null, null);

        Response response = order.execute(new Request("a", "a", "a", 0));

        assertEquals(BusinessError.InvalidPhoneNumber, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_error_out_when_customer_does_not_exist() {

        class StubCustomerRepo implements Customers {

            @Override
            public boolean doesNotExist(String phoneNumber) {
                return true;
            }

            @Override
            public char[] getPasswordHash(String phoneNumber) {
                return null;
            }
        }

        InputBoundary order = new Factory().getInstance(new StubTodaysMenu( false, 0, ""),
                                                        new StubCustomerRepo(),
                                                        new PhoneNumberValidator(),
                                                        null,
                                                        new Config(false));

        Response response = order.execute(new Request("a", "a", "a", 2));

        assertEquals(BusinessError.CustomerDoesNotExist, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_error_out_when_password_is_invalid() {
        InputBoundary order = new Factory().getInstance(new StubTodaysMenu(false, 0, ""),
                                                        new StubCustomers(),
                                                        new PhoneNumberValidator(),
                                                        null,
                                                        new Config(false));

        Response response = order.execute(new Request("a", "a", "a", 2));

        assertEquals(BusinessError.InvalidPassword, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_error_out_when_menu_item_does_not_exist() {

        InputBoundary order = new Factory().getInstance(new StubTodaysMenu(false, 0, ""),
                                                        new StubCustomers(),
                                                        new PhoneNumberValidator(),
                                                        null,
                                                        new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", 2));

        assertEquals(BusinessError.MenuItemDoesNotExist, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }


    @Test
    void should_error_out_when_meal_cannot_be_served_right_now() {

        InputBoundary order = new Factory().getInstance(new StubTodaysMenu(true, 0, ""),
                                                        new StubCustomers(),
                                                        new PhoneNumberValidator(),
                                                        null,
                                                        new Config(true));

        Response response = order.execute(new Request("a", "test123", "a", 2));

        assertEquals(BusinessError.CannotServeMealTypeRightNow, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_error_out_when_available_quantity_is_less_than_requested() {

        InputBoundary order = new Factory().getInstance(new StubTodaysMenu(true, 1, ""),
                                                        new StubCustomers(),
                                                        new PhoneNumberValidator(),
                                                        null,
                                                        new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", 2));

        assertEquals(BusinessError.AvailableQuantityLessThanRequestedQuantity, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_succeed_for_valid_input() {

        class Orders implements TodaysOrders {

            @Override
            public String add(String menuID, int quantity) {
                assertTrue(true, "OrderRepository.add() should be called");
                return "1";
            }
        }

        StubTodaysMenu menuRepository = new StubTodaysMenu(true, 2, "a");
        InputBoundary order = new Factory().getInstance(menuRepository,
                new StubCustomers(),
                new PhoneNumberValidator(),
                new Orders(),
                new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", 2));

        assertNull(response.error());
        assertTrue(menuRepository.getQuantity("").isPresent());
        // id is any random int. The stub doesn't check it anyway
        assertEquals(0, menuRepository.getQuantity("").get());
        assertEquals("1", response.getOrderID());
        assertNotNull(response.getRequestID());
        assertNotNull(response.getOrderID());
    }

    @Test
    void should_return_error_if_updateQuantity_throws_SQLException() {
        final int quantity = 2;
        InputBoundary order = new Factory().getInstance(new TodaysMenu() {

            @Override
            public Optional<Integer> getQuantity(String menuID) {
                return Optional.of(quantity + 1);
            }

            @Override
            public void updateQuantity(String menuID, int updatedQuantity) throws SQLException {
                throw new SQLException();
            }

            @Override
            public String getMealType(String menuID) {
                return null;
            }
        }, new StubCustomers(), new PhoneNumberValidator(), null, new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", quantity));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_return_error_if_getQuantity_throws_SQLException() {
        InputBoundary order = new Factory().getInstance(new TodaysMenu() {

            @Override
            public Optional<Integer> getQuantity(String menuID) throws SQLException {
                throw new SQLException();
            }

            @Override
            public void updateQuantity(String menuID, int updatedQuantity) {

            }

            @Override
            public String getMealType(String menuID) throws SQLException {
                throw new SQLException();
            }
        }, new StubCustomers(), new PhoneNumberValidator(), null, new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", 2));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_return_error_if_add_throws_SQLException() {
        InputBoundary order = new Factory().getInstance(new StubTodaysMenu(true,2, "breakfast"),
                new StubCustomers(),
                new PhoneNumberValidator(),
                (id, quantity) -> {
                    throw new SQLException();
                }, new Config(false));

        Response response = order.execute(new Request("a", "test123", "a", 1));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }

    @Test
    void should_return_error_if_authentication_returns_unexpected_failure() {
        InputBoundary order = new Factory().getInstance(null,
                new Customers() {
                    @Override
                    public boolean doesNotExist(String phoneNumber) throws SQLException {
                        throw new SQLException();
                    }

                    @Override
                    public char[] getPasswordHash(String phoneNumber) {
                        return new char[0];
                    }
                }, new PhoneNumberValidator(), null,null);

        Response response = order.execute(new Request("a", "test123", "a", 1));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
        assertNull(response.getOrderID());
    }
}

package authenticate;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import use_cases.authenticate.*;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class StubRepository implements Customers {
    boolean doesNotExist;
    char[] passwordHash;

    public StubRepository(boolean doesNotExist, char[] passwordHash) {
        this.doesNotExist = doesNotExist;
        this.passwordHash = passwordHash;
    }

    public StubRepository(boolean doesNotExist) {
        this(doesNotExist, BCrypt.withDefaults().hashToChar(12, "test123".toCharArray()) );
    }

    @Override
    public boolean doesNotExist(String phoneNumber) {
        return doesNotExist;
    }

    @Override
    public char[] getPasswordHash(String phoneNumber) {
        return passwordHash;
    }
}

class PhoneNumberValidator implements use_cases.util.PhoneNumberValidator {
    boolean isInvalid;

    public PhoneNumberValidator(boolean isInvalid) {
        this.isInvalid = isInvalid;
    }

    @Override
    public boolean isInvalid(String phoneNumber) {
        return isInvalid;
    }
}

public class ImplTest {
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_if_id_is_null_or_empty(String id) {
        InputBoundary authenticate = new Factory().getInstance(null, null);
        Response response = authenticate.execute(new Request(id, " wlfh"));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_if_password_is_null_or_empty(String password) {
        InputBoundary authenticate = new Factory().getInstance(null, null);
        Response response = authenticate.execute(new Request(" wlfh", password));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_error_if_phone_number_is_invalid() {
        InputBoundary authenticate = new Factory().getInstance(new StubRepository(true), new PhoneNumberValidator(true));
        Response response = authenticate.execute(new Request("a", "test"));

        assertEquals(BusinessError.InvalidPhoneNumber, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_error_if_customer_does_not_exist() {
        InputBoundary authenticate = new Factory().getInstance(new StubRepository(true), new PhoneNumberValidator(false));
        Response response = authenticate.execute(new Request("a", "test"));

        assertEquals(BusinessError.CustomerDoesNotExist, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_error_when_password_is_wrong() {

        InputBoundary authenticate = new Factory().getInstance(new StubRepository(false), new PhoneNumberValidator(false));
        Response response = authenticate.execute(new Request("a", "test12"));

        assertEquals(BusinessError.InvalidPassword, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_unexpected_failure_when_doesNotExist_throws_SQLException() {
        InputBoundary authenticate = new Factory().getInstance(new Customers() {
            @Override
            public boolean doesNotExist(String phoneNumber) throws SQLException {
                throw new SQLException();
            }

            @Override
            public char[] getPasswordHash(String phoneNumber) {
                return new char[0];
            }
        }, new PhoneNumberValidator(false));

        Response response = authenticate.execute(new Request("a", "test12"));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void success() {

        InputBoundary authenticate = new Factory().getInstance(new StubRepository(false), new PhoneNumberValidator(false));
        Response response = authenticate.execute(new Request("a", "test123"));

        assertNull(response.error());
        assertNotNull(response.getRequestID());
    }
}

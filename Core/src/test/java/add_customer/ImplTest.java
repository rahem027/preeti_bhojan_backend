package add_customer;

import entities.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import use_cases.add_customer.*;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class StubRepository implements Customers {

    boolean contains;

    public StubRepository(boolean contains) {
        this.contains = contains;
    }

    @Override
    public boolean exists(String phoneNumber) {
        return contains;
    }

    @Override
    public void add(Customer customer, char[] hashedPassword) { }
}

class PhoneNumberValidator implements use_cases.util.PhoneNumberValidator {
    boolean isInvalid;

    public PhoneNumberValidator(boolean isInvalid) {
        this.isInvalid = isInvalid;
    }

    @Override
    public boolean isInvalid(String phoneNumber) {
        return isInvalid;
    }
}
public class ImplTest {

    private static final String validPhoneNumber = "0000000000";

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_name_is_null_or_empty(String name) {
        InputBoundary addCustomer = new Factory().getInstance(null, null);

        Response response = addCustomer.execute(new Request(name, "a", "a", "a", "a"));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_address_is_null_or_empty(String address) {
        InputBoundary addCustomer = new Factory().getInstance(null, null);

        Response response = addCustomer.execute(new Request("a", address, "a", "a", "a"));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_company_is_null_or_empty(String company) {
        InputBoundary addCustomer = new Factory().getInstance(null, null);

        Response response = addCustomer.execute(new Request("a", "a", company, "a", "a"));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_phoneNumber_is_null_or_empty(String phoneNumber) {
        InputBoundary addCustomer = new Factory().getInstance(null, null);

        Response response = addCustomer.execute(new Request("a", "a", "a", phoneNumber, "a"));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   ", "\t", "\n"})
    void should_return_error_when_password_is_null_or_empty(String password) {
        InputBoundary addCustomer = new Factory().getInstance(null, null);

        Response response = addCustomer.execute(new Request("a", "a", "a", "a", password));

        assertEquals(BusinessError.InvalidInput, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_error_when_phone_number_is_invalid() {
        InputBoundary addCustomer = new Factory().getInstance(new StubRepository(false), new PhoneNumberValidator(true));

        Response response = addCustomer.execute(new Request("test1", "abc", "test", "a", "password"));

        assertEquals(BusinessError.InvalidPhoneNumber, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_error_when_customer_already_exists() {
        InputBoundary addCustomer = new Factory().getInstance(new StubRepository(true), new PhoneNumberValidator(false));

        Response response = addCustomer.execute(new Request("test1", "abc", "test", validPhoneNumber, "password"));

        assertEquals(BusinessError.CustomerAlreadyExists, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_unexpected_failure_when_exists_throws_SQLException() {
        InputBoundary addCustomer = new Factory().getInstance(new Customers() {
            @Override
            public boolean exists(String phoneNumber) throws SQLException {
                throw new SQLException();
            }

            @Override
            public void add(Customer customer, char[] hashedPassword) { }
        }, new PhoneNumberValidator(false));

        Response response = addCustomer.execute(new Request("test1", "abc", "12986", validPhoneNumber, "password"));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void should_return_unexpected_failure_when_add_throws_SQLException() {
        InputBoundary addCustomer = new Factory().getInstance(new Customers() {
            @Override
            public boolean exists(String phoneNumber) {
                return false;
            }

            @Override
            public void add(Customer customer, char[] hashedPassword) throws SQLException {
                throw new SQLException();
            }
        }, new PhoneNumberValidator(false));

        Response response = addCustomer.execute(new Request("test1", "abc", "12986", validPhoneNumber, "password"));

        assertEquals(BusinessError.UnexpectedFailure, response.error());
        assertNotNull(response.getRequestID());
    }

    @Test
    void success() {
        InputBoundary addCustomer = new Factory().getInstance(new StubRepository(false), new PhoneNumberValidator(false));

        Response response = addCustomer.execute(new Request("test1", "abc", "test", validPhoneNumber, "password"));

        assertNull(response.error());
        assertNotNull(response.getRequestID());
    }
}

package use_cases.add_customer;

import use_cases.util.PhoneNumberValidator;

public class Factory {
    public InputBoundary getInstance(Customers customers, PhoneNumberValidator phoneNumberValidator) {
        return new Impl(customers, phoneNumberValidator);
    }
}

package use_cases.add_customer;

import entities.Customer;

import java.sql.SQLException;

public interface Customers {
    boolean exists(String phoneNumber) throws SQLException;
    void add(Customer customer, char[] hashedPassword) throws SQLException;
}

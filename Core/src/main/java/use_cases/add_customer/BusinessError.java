package use_cases.add_customer;

public enum BusinessError {
    InvalidInput,
    CustomerAlreadyExists,
    InvalidPhoneNumber,
    UnexpectedFailure
}

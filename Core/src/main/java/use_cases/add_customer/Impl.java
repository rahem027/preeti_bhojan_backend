package use_cases.add_customer;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;
import entities.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import use_cases.util.PhoneNumberValidator;

import java.sql.SQLException;
import java.util.UUID;

import static use_cases.util.StringUtil.empty;

public class Impl implements InputBoundary {
    private Customers customers;
    private PhoneNumberValidator phoneNumberValidator;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public Impl(Customers customers, PhoneNumberValidator phoneNumberValidator) {
        this.customers = customers;
        this.phoneNumberValidator = phoneNumberValidator;
    }

    @Override
    public Response execute(Request request) {
        UUID requestID = request.getRequestID();

        Response response = new Response(requestID);

        try {
            if (empty(request.getName()) ||
                empty(request.getAddress()) ||
                empty(request.getCompany()) ||
                empty(request.getPhoneNumber()) ||
                empty(request.getPassword())) {

                response.error(BusinessError.InvalidInput);
                logger.info("Invalid input submitted: {}", requestID);

            } else if (phoneNumberValidator.isInvalid(request.getPhoneNumber())) {
                response.error(BusinessError.InvalidPhoneNumber);
                logger.info("Invalid phone number submitted: {}", requestID);

            } else if (customers.exists(request.getPhoneNumber())) {
                response.error(BusinessError.CustomerAlreadyExists);
                logger.info("Customer already exists: {}", requestID);
            } else {

                char[] hashedPassword = BCrypt.with(LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2A)).hashToChar(12, request.getPassword().toCharArray());
                customers.add(new Customer(request.getName(), request.getAddress(), request.getCompany(), request.getPhoneNumber()), hashedPassword);
                logger.info("Success: {}", requestID);

            }
        } catch (SQLException e) {
            response.error(BusinessError.UnexpectedFailure);
            logger.info("Unexpected Failure {}", requestID, e);
        }

        return response;
    }
}

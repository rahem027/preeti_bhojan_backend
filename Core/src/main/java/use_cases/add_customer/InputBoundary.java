package use_cases.add_customer;

public interface InputBoundary {
    Response execute(Request request);
}

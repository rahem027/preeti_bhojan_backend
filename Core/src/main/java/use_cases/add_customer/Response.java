package use_cases.add_customer;

import java.util.UUID;

public class Response {
    private BusinessError businessError;
    private UUID requestID;

    public Response(UUID requestID) {
        this.requestID = requestID;
    }

    public BusinessError error() {
        return businessError;
    }

    public void error(BusinessError businessError) {
        this.businessError = businessError;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

package use_cases.add_customer;

import java.util.UUID;

public class Request {
    private String name, address, company, phoneNumber, password;
    private UUID requestID;

    public Request(String name, String address, String company, String phone_number, String password) {
        this.name = name;
        this.address = address;
        this.company = company;
        this.phoneNumber = phone_number;
        this.password = password;
        this.requestID = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCompany() {
        return company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

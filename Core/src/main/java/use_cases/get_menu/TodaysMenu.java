package use_cases.get_menu;

import entities.MenuItem;

import java.sql.SQLException;
import java.util.List;

public interface TodaysMenu {
    List<MenuItem> get(String mealType) throws SQLException;
}

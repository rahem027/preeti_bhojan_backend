package use_cases.get_menu;

public interface InputBoundary {
    Response execute(Request request);
}

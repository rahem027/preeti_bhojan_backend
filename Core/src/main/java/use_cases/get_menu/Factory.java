package use_cases.get_menu;

public class Factory {
    public InputBoundary getInstance(TodaysMenu menu, Config config) {
        return new Impl(menu, config);
    }
}

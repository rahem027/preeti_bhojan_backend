package use_cases.get_menu;

import entities.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Response {
    private List<MenuItem> menu = new ArrayList<>();
    private BusinessError error;
    private UUID requestID;

    public Response(UUID requestID) {
        this.requestID = requestID;
    }

    public void setMenu(List<MenuItem> menu) {
        this.menu = menu;
    }

    public List<MenuItem> getMenu() {
        return menu;
    }

    public BusinessError error() {
        return error;
    }

    public void error(BusinessError error) {
        this.error = error;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

package use_cases.get_menu;

public enum BusinessError {
    InvalidInput,
    InvalidMealType,
    UnexpectedFailure
}

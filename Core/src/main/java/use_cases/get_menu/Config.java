package use_cases.get_menu;

public interface Config {
    boolean isNotValid(String mealType);
}

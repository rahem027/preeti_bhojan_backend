package use_cases.get_menu;

import java.util.UUID;

public class Request {
    private String mealType;
    private UUID requestID;

    public Request(String mealType) {
        this.mealType = mealType;
        this.requestID = UUID.randomUUID();
    }

    public String getMealType() {
        return mealType;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

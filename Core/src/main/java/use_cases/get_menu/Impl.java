package use_cases.get_menu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.UUID;

import static use_cases.util.StringUtil.empty;

class Impl implements InputBoundary {

    private TodaysMenu menu;
    private Config config;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public Impl(TodaysMenu menu, Config config) {
        this.menu = menu;
        this.config = config;
    }

    @Override
    public Response execute(Request request) {
        UUID requestID = request.getRequestID();
        Response response = new Response(requestID);

        if (empty(request.getMealType())) {
            response.error(BusinessError.InvalidInput);
            logger.info("Invalid input: {}", requestID);

        } else if (config.isNotValid(request.getMealType())) {
            response.error(BusinessError.InvalidMealType);
            logger.info("Invalid meal type: {}", requestID);

        } else {
            try {
                response.setMenu(menu.get(request.getMealType()));
                logger.info("success {}", requestID);

            } catch (SQLException e) {
                response.error(BusinessError.UnexpectedFailure);
                logger.info("Unexpected failure {}", requestID, e);
            }
        }

        return response;
    }
}

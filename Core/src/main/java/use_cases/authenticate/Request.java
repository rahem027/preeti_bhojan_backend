package use_cases.authenticate;

import java.util.UUID;

public class Request {
    private String phoneNumber, password;
    private UUID requestID;

    public Request(String phoneNumber, String password) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.requestID = UUID.randomUUID();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

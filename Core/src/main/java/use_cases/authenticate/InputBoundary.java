package use_cases.authenticate;

public interface InputBoundary {
    Response execute(Request request);
}

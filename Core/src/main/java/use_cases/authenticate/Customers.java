package use_cases.authenticate;

import java.sql.SQLException;

public interface Customers {
    boolean doesNotExist(String phoneNumber) throws SQLException;
    char[] getPasswordHash(String phoneNumber) throws SQLException;
}

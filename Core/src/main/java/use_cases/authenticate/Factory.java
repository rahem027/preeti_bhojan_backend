package use_cases.authenticate;

import use_cases.util.PhoneNumberValidator;

public class Factory {
    public InputBoundary getInstance(Customers repo, PhoneNumberValidator phoneNumberValidator) {
        return new Impl(repo, phoneNumberValidator);
    }
}

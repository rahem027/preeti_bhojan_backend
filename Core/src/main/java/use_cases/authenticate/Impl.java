package use_cases.authenticate;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import use_cases.util.PhoneNumberValidator;

import java.sql.SQLException;
import java.util.UUID;

import static use_cases.util.StringUtil.empty;

public class Impl implements InputBoundary {
    private Customers customers;
    private PhoneNumberValidator phoneNumberValidator;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public Impl(Customers customers, PhoneNumberValidator phoneNumberValidator) {
        this.customers = customers;
        this.phoneNumberValidator = phoneNumberValidator;
    }

    @Override
    public Response execute(Request request)  {
        UUID requestID = request.getRequestID();
        Response response = new Response(requestID);

        try {
            if (empty(request.getPhoneNumber()) || empty(request.getPassword())) {
                response.error(BusinessError.InvalidInput);
                logger.info("Invalid input: {}", requestID);

            } else if (phoneNumberValidator.isInvalid(request.getPhoneNumber())) {
                response.error(BusinessError.InvalidPhoneNumber);
                logger.info("Invalid phone number: {}", requestID);

            } else if (customers.doesNotExist(request.getPhoneNumber())) {
               response.error(BusinessError.CustomerDoesNotExist);
               logger.info("Customer does not exist: {}", requestID);

            } else {
                final BCrypt.Result verify = BCrypt.verifyer(BCrypt.Version.VERSION_2A, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2A))
                        .verify(request.getPassword().toCharArray(), customers.getPasswordHash(request.getPhoneNumber()));

                if (!verify.verified) {
                    response.error(BusinessError.InvalidPassword);
                    logger.info("Invalid password: {}", requestID);
                }
            }

            logger.info("Success: {}", requestID);
        } catch (SQLException e) {
            response.error(BusinessError.UnexpectedFailure);
        }

        return response;
    }
}

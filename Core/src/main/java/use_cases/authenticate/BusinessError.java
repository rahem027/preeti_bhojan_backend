package use_cases.authenticate;

public enum BusinessError {
    InvalidInput,
    CustomerDoesNotExist,
    InvalidPassword,
    InvalidPhoneNumber,
    UnexpectedFailure,
}

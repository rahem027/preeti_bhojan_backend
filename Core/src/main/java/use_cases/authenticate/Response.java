package use_cases.authenticate;

import java.util.UUID;

public class Response {
    private BusinessError error;
    private UUID requestID;

    public Response(UUID requestID) {
        this.requestID = requestID;
    }

    public BusinessError error() {
        return error;
    }

    public void error(BusinessError error) {
        this.error = error;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

package use_cases.util;

public class StringUtil {
    public static boolean empty(String s) {
        return s == null || s.isBlank();
    }
}

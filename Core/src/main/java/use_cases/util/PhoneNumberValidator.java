package use_cases.util;

public interface PhoneNumberValidator {
    boolean isInvalid(String phoneNumber);
}

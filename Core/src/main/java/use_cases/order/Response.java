package use_cases.order;

import java.util.UUID;

public class Response {
    private String orderID;
    private UUID requestID;
    private BusinessError error;

    public Response(UUID requestID) {
        this.requestID = requestID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public BusinessError error() {
        return error;
    }

    public void error(BusinessError error) {
        this.error = error;
    }

    public UUID getRequestID() {
        return requestID;
    }
}

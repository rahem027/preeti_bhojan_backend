package use_cases.order;

import java.sql.SQLException;
import java.util.Optional;

public interface TodaysMenu {
    Optional<Integer> getQuantity(String menuID) throws SQLException;
    void updateQuantity(String menuID, int updatedQuantity) throws SQLException;
    String getMealType(String menuID) throws SQLException;
}

package use_cases.order;

public interface Config {
    boolean cannotServeMealTypeRightNow(String mealType);
}

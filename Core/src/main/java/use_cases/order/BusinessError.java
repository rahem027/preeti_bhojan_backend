package use_cases.order;

public enum BusinessError {
    // Authentication Error
    CustomerDoesNotExist,
    InvalidPhoneNumber,
    InvalidPassword,

    // BusinessError
    InvalidInput,
    MenuItemDoesNotExist,
    AvailableQuantityLessThanRequestedQuantity,
    CannotServeMealTypeRightNow,
    UnexpectedFailure
}

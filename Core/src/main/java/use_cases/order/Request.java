package use_cases.order;

import java.util.UUID;

public class Request {
    private int  quantity;
    private String phoneNumber, password, menuID;
    private UUID requestID;

    public Request(String phoneNumber, String password, String menuID, int quantity) {
        this.phoneNumber = phoneNumber;
        this.menuID = menuID;
        this.quantity = quantity;
        this.password = password;
        this.requestID = UUID.randomUUID();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPassword() {
        return password;
    }

    public UUID getRequestID() {
        return requestID;
    }

    public String getMenuID() {
        return menuID;
    }
}

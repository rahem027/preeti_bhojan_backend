package use_cases.order;

import use_cases.authenticate.Customers;
import use_cases.util.PhoneNumberValidator;

public class Factory {
    public InputBoundary getInstance(TodaysMenu todaysMenu,
                                     Customers customers,
                                     PhoneNumberValidator phoneNumberValidator,
                                     TodaysOrders todaysOrders,
                                     Config config) {
        return new Impl(todaysMenu, customers, phoneNumberValidator, todaysOrders, config);
    }
}

package use_cases.order;

import java.sql.SQLException;

public interface TodaysOrders {
    String add(String menuID, int quantity) throws SQLException;
}


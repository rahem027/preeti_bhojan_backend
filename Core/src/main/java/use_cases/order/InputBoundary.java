package use_cases.order;

public interface InputBoundary {
    Response execute(Request request);
}

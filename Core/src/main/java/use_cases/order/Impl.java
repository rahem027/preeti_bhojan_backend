package use_cases.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import use_cases.authenticate.Customers;
import use_cases.authenticate.Factory;
import use_cases.util.PhoneNumberValidator;

import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

import static use_cases.util.StringUtil.empty;

class Impl implements InputBoundary {

    private TodaysMenu todaysMenu;
    private Customers customers;
    private TodaysOrders todaysOrders;
    private Config config;
    private PhoneNumberValidator phoneNumberValidator;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public Impl(TodaysMenu todaysMenu, Customers customers, PhoneNumberValidator phoneNumberValidator, TodaysOrders todaysOrders, Config config) {
        this.todaysMenu = todaysMenu;
        this.phoneNumberValidator = phoneNumberValidator;
        this.config = config;
        this.customers = customers;
        this.todaysOrders = todaysOrders;
    }

    @Override
    public Response execute(Request request) {
        UUID requestID = request.getRequestID();
        Response response = new Response(requestID);

        if (empty(request.getMenuID()) || request.getQuantity() < 0) {
            response.error(BusinessError.InvalidInput);
            logger.info("Invalid input {}", requestID);

            return response;
        }

        use_cases.authenticate.InputBoundary authenticate = new Factory().getInstance(customers, phoneNumberValidator);
        use_cases.authenticate.Response authenticateResponse = authenticate.execute(new use_cases.authenticate.Request(request.getPhoneNumber(), request.getPassword()));

        if (authenticateResponse.error() != null) {

            switch (authenticateResponse.error()) {
                case InvalidInput: response.error(BusinessError.InvalidInput);
                break;

                case InvalidPhoneNumber: response.error(BusinessError.InvalidPhoneNumber);
                break;

                case CustomerDoesNotExist: response.error(BusinessError.CustomerDoesNotExist);
                break;

                case InvalidPassword: response.error(BusinessError.InvalidPassword);
                break;

                case UnexpectedFailure: response.error(BusinessError.UnexpectedFailure);
                break;
            }

            logger.info("Authentication failure: {}", requestID);
            return response;
        }


        try {
            Optional<Integer> quantity = todaysMenu.getQuantity(request.getMenuID());

            if (quantity.isEmpty()) {
                response.error(BusinessError.MenuItemDoesNotExist);
                logger.info("Menu item does not exist {}", requestID);

            } else if (config.cannotServeMealTypeRightNow(todaysMenu.getMealType(request.getMenuID()))) {
                response.error(BusinessError.CannotServeMealTypeRightNow);
                logger.info("Cannot server meal right now {}", requestID);

            } else if (quantity.get() < request.getQuantity()) {
                response.error(BusinessError.AvailableQuantityLessThanRequestedQuantity);
                logger.info("Available quantity less than requested {}", requestID);

            } else {
                todaysMenu.updateQuantity(request.getMenuID(), quantity.get() - request.getQuantity() );
                String orderID = todaysOrders.add(request.getPhoneNumber(), request.getQuantity());

                response.setOrderID(orderID);
                logger.info("Success. Order ID: {}", orderID);
            }
        } catch (SQLException e) {
            response.error(BusinessError.UnexpectedFailure);
            logger.info("Unexpected failure {}", requestID, e);
        }

        return response;
    }
}

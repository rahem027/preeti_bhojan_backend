package entities;

public class Customer {
    String name, address, company, phoneNumber;

    public Customer(String name, String address, String company, String phoneNumber) {
        this.name = name;
        this.address = address;
        this.company = company;
        this.phoneNumber = phoneNumber;
    }

    /*
    * Used by repositories to store data
    */
    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCompany() {
        return company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
